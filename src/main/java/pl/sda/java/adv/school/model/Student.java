package pl.sda.java.adv.school.model;

import java.time.LocalDate;

public class Student extends Person{

    private short startYear;
    private byte schoolYear;



    public short getStartYear() {
        return startYear;
    }

    public void setStartYear(short startYear) {
        this.startYear = startYear;
    }

    public byte getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(byte schoolYear) {
        this.schoolYear = schoolYear;
    }



    @Override
    public String toString() {
        return "Student{" +
                "startYear=" + startYear +
                ", schoolYear=" + schoolYear +
                '}';
    }
}
